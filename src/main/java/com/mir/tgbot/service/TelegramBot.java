package com.mir.tgbot.service;

import com.mir.tgbot.config.BotConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
@Slf4j
@Component
public class TelegramBot extends TelegramLongPollingBot {
    final BotConfig config;
    public TelegramBot(BotConfig config){
        this.config = config;
    }
    @Override
    public void onUpdateReceived(Update update) {
        if(update.hasMessage() && update.getMessage().hasText()){
            String messageText = update.getMessage().getText();
            long chatId = update.getMessage().getChatId();
            switch (messageText){
                case "/start":
                    startCommandRecieved(chatId,update.getMessage().getChat().getFirstName());
                    break;
                default: sendMessage(chatId, "I don't understand you. Try again!");


            }
        }
    }
    @Override
    public String getBotUsername() {
        return config.getBotName();
    }
    @Override
    public String getBotToken() {
        return config.getToken();
    }
    private void startCommandRecieved(long chatId, String name){
        String answer = "Welcome, " + name + "!" + " Do you want to buy some chip Steam games?";
        sendMessage(chatId, answer);
        log.info("Replied to user: " + name);
    }
    private void sendMessage(long chatId, String textToSend){
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        message.setText(textToSend);
        try {
            execute(message);
        }
        catch (TelegramApiException e){
                log.error("Error occurred: " + e.getMessage());
        }
    }
}
